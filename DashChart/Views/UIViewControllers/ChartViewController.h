//
//  ChartViewController.h
//  DashChart
//
//  Created by Andris Zalitis on 06/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Charts;
#import "ChartView.h"

@interface ChartViewController : UIViewController <ChartView, IChartAxisValueFormatter>

@end

