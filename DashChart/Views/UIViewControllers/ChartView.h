//
//  ChartView.h
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

@protocol ChartView<NSObject>

- (void)presentChartWithYValues:(NSArray *)yValues xValues:(NSArray *)xValues;
- (void)showLoadingIndicator;
- (void)hideLoadingIndicator;

@end

