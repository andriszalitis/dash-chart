//
//  ChartViewController.m
//  DashChart
//
//  Created by Andris Zalitis on 06/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import "ChartViewController.h"
#import "UIColor+Hex.h"
#import "ChartYValue.h"
#import "ChartPresenter.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "TimePeriodSegmentedControl.h"

@interface ChartViewController ()

@property (nonatomic, weak) IBOutlet CombinedChartView *chartView;
@property (nonatomic, weak) IBOutlet TimePeriodSegmentedControl *timeIntervalSegmentedControl;
@property (nonatomic, weak) IBOutlet TimePeriodSegmentedControl *barWidthSegmentedControl;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (nonatomic, strong) ChartPresenter *chartPresenter;
@property (nonatomic, assign) NSInteger numberOfDataItems;
@property (nonatomic, copy) NSArray *xValues;

@end

@implementation ChartViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configureChart];
    [self configureTimeIntervalSegmentedControl];
    [self configureBarWidthSegmentedControl];

    self.chartPresenter = [[ChartPresenter alloc] initWithSelectedTimeInterval:[self.timeIntervalSegmentedControl selectedTimePeriod]
                                                              selectedBarWidth:[self.barWidthSegmentedControl selectedTimePeriod]];
    self.chartPresenter.chartView = self;
    [self.chartPresenter start];
}

- (void)configureChart
{
    self.chartView.legend.enabled = NO;
    self.chartView.chartDescription.enabled = NO;
    // auto scale causes flicker when scroll released (at least on sim)
    self.chartView.autoScaleMinMaxEnabled = NO;

    self.chartView.xAxis.labelPosition = XAxisLabelPositionBottom;
    self.chartView.xAxis.drawGridLinesEnabled = NO;
    self.chartView.xAxis.labelCount = 7;
    self.chartView.xAxis.valueFormatter = self;

    self.chartView.leftAxis.axisMinimum = 0;
    // this just hides axis labels, but it's min and max values are still used
    // for calculating bar heights
    self.chartView.leftAxis.enabled = NO;

    self.chartView.rightAxis.drawGridLinesEnabled = NO;

    [self.chartView setScaleYEnabled:NO];
}

static NSInteger secondsInMinute = 60;
static NSInteger secondsInHour = 60 * 60;

- (void)configureTimeIntervalSegmentedControl
{
    self.timeIntervalSegmentedControl.timePeriods = @[
                                                      [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"6h", @"")
                                                                                            number:@(secondsInHour * 6)],
                                                      [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"24h", @"")
                                                                                            number:@(secondsInHour * 24)],
                                                      [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"2d", @"")
                                                                                            number:@(secondsInHour * 24 * 2)],
                                                      [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"4d", @"")
                                                                                            number:@(secondsInHour * 24 * 4)],
                                                      [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"1w", @"")
                                                                                            number:@(secondsInHour * 24 * 7)],
                                                      [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"2w", @"")
                                                                                            number:@(secondsInHour * 24 * 7 * 2)],
                                                      [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"1m", @"")
                                                                                            number:@(secondsInHour * 24 * 30)],
                                                      [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"3m", @"")
                                                                                            number:@(secondsInHour * 24 * 30 * 3)],
                                                      ];
    self.timeIntervalSegmentedControl.selectedSegmentIndex = 0;
}

- (void)configureBarWidthSegmentedControl
{
    self.barWidthSegmentedControl.timePeriods = @[
                                                  [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"5m", @"")
                                                                                        number:@(secondsInMinute * 5)],
//                                                  [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"15m", @"")
//                                                                                        number:@(secondsInMinute * 15)],
//                                                  [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"30m", @"")
//                                                                                        number:@(secondsInMinute * 30)],
//                                                  [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"2h", @"")
//                                                                                        number:@(secondsInHour * 2)],
//                                                  [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"4h", @"")
//                                                                                        number:@(secondsInHour * 4)],
//                                                  [StringNumberPair stringNumberPairWithString:NSLocalizedString(@"1d", @"")
//                                                                                        number:@(secondsInHour * 24)],
                                                  ];
    self.barWidthSegmentedControl.selectedSegmentIndex = 0;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self limitNumberOfVisibleBars];
}

- (void)limitNumberOfVisibleBars
{
    [self.chartView setVisibleXRangeMaximum:40];
    [self.chartView moveViewToX:self.numberOfDataItems - 1];
    // this fixes issue with zoom, when changing from bigger range to smaller
    [self.chartView zoomWithScaleX:-1 scaleY:-1 x:0 y:0];
}

#pragma mark - IChartAxisValueFormatter

- (NSString *)stringForValue:(double)value
                        axis:(ChartAxisBase *)axis
{
    static dispatch_once_t onceToken;
    static NSDateFormatter *dateFormatter;
    dispatch_once(&onceToken, ^{
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateStyle = NSDateFormatterShortStyle;
        dateFormatter.timeStyle = NSDateFormatterShortStyle;
    });
    NSInteger index = (NSInteger)value;
    NSInteger xValue = [self.xValues[index] integerValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:xValue];
    return [dateFormatter stringFromDate:date];
}

#pragma mark - IBActions

- (IBAction)didChangeValueOfTimeInterval:(id)sender
{
    [self.chartPresenter didChangeValueOfTimeIntervalTo:[self.timeIntervalSegmentedControl selectedTimePeriod]];
}

- (IBAction)didChangeValueOfBarWidth:(id)sender
{
    [self.chartPresenter didChangeValueOfBarWidthTo:[self.barWidthSegmentedControl selectedTimePeriod]];
}

#pragma mark - ChartsView

- (void)presentChartWithYValues:(NSArray *)yValues xValues:(NSArray *)xValues
{
    CombinedChartData *data = [[CombinedChartData alloc] init];
    data.candleData = [self generateCandleDataFromYValues:yValues];
    data.barData = [self generateVolumeDataFromYValues:yValues];

    [self.chartView.leftAxis resetCustomAxisMax];
    self.chartView.data = data;
    self.xValues = xValues;

    // we want volume bars to take only the lower part of the chart
    self.chartView.leftAxis.axisMaximum = self.chartView.leftAxis.axisMaximum * 1.5;
    [self.chartView setNeedsDisplay];

    self.numberOfDataItems = yValues.count;
    [self limitNumberOfVisibleBars];
}

- (CandleChartData *)generateCandleDataFromYValues:(NSArray *)yValues
{
    NSMutableArray *dataEntries = [NSMutableArray new];
    NSInteger i = 0;
    for (ChartYValue *yValue in yValues) {
        CandleChartDataEntry *dataEntry = [[CandleChartDataEntry alloc]
                                           initWithX:i
                                           shadowH:yValue.high
                                           shadowL:yValue.low
                                           open:yValue.open
                                           close:yValue.close];
        [dataEntries addObject:dataEntry];
        i += 1;
    }

    CandleChartDataSet *dataSet = [[CandleChartDataSet alloc] initWithValues:dataEntries label:nil];
    dataSet.axisDependency = AxisDependencyRight;

    dataSet.drawIconsEnabled = NO;
    dataSet.drawValuesEnabled = NO;

    dataSet.shadowColor = UIColor.darkGrayColor;
    dataSet.shadowWidth = 2;
    dataSet.decreasingColor = [UIColor colorWithRGBHexValue:0xad191f];
    dataSet.decreasingFilled = YES;
    dataSet.increasingColor = [UIColor colorWithRGBHexValue:0x277525];
    dataSet.increasingFilled = YES;
    dataSet.neutralColor = UIColor.darkGrayColor;

    return [[CandleChartData alloc] initWithDataSet:dataSet];
}

- (BarChartData *)generateVolumeDataFromYValues:(NSArray *)yValues
{
    NSMutableArray *dataEntries = [NSMutableArray new];
    NSInteger i = 0;
    for (ChartYValue *yValue in yValues) {
        [dataEntries addObject:[[BarChartDataEntry alloc] initWithX:i y:yValue.volume]];
        i += 1;
    }

    BarChartDataSet *dataSet = [[BarChartDataSet alloc] initWithValues:dataEntries label:nil];
    dataSet.axisDependency = AxisDependencyLeft;

    [dataSet setColor:[UIColor colorWithRGBHexValue:0xdddddd]];
    dataSet.drawIconsEnabled = NO;
    dataSet.drawValuesEnabled = NO;

    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:dataSet];

    BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];

    data.barWidth = 0.9f;
    return data;
}

- (void)showLoadingIndicator
{
    self.chartView.alpha = 0;
    [self.loadingIndicator startAnimating];
}

- (void)hideLoadingIndicator
{
    self.chartView.alpha = 1;
    [self.loadingIndicator stopAnimating];
}

- (void)dealloc
{
    [self.chartPresenter stop];
}

@end
