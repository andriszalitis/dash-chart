//
//  StringNumberPair.h
//  DashChart
//
//  Created by Andris Zalitis on 08/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringNumberPair: NSObject

@property (nonatomic, copy) NSString *string;
@property (nonatomic, copy) NSNumber *number;

+ (instancetype)stringNumberPairWithString:(NSString *)string number:(NSNumber *)number;

@end
