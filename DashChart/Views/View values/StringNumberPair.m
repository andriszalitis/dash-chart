//
//  StringNumberPair.m
//  DashChart
//
//  Created by Andris Zalitis on 08/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import "StringNumberPair.h"

@implementation StringNumberPair

+ (instancetype)stringNumberPairWithString:(NSString *)string number:(NSNumber *)number
{
    StringNumberPair *pair = [StringNumberPair new];
    pair.string = string;
    pair.number = number;
    return pair;
}

@end

