//
//  TimePeriodSegmentedControl.h
//  DashChart
//
//  Created by Andris Zalitis on 08/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StringNumberPair.h"

@interface TimePeriodSegmentedControl : UISegmentedControl

@property (nonatomic, copy) NSArray *timePeriods;

- (NSInteger)selectedTimePeriod;

@end
