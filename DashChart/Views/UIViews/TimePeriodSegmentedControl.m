//
//  TimePeriodSegmentedControl.m
//  DashChart
//
//  Created by Andris Zalitis on 08/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import "TimePeriodSegmentedControl.h"
#import "StringNumberPair.h"

@interface TimePeriodSegmentedControl ()

@end

@implementation TimePeriodSegmentedControl

- (void)setTimePeriods:(NSArray *)timePeriods
{
    _timePeriods = [timePeriods copy];
    [self removeAllSegments];
    NSInteger i = 0;
    for (StringNumberPair *pair in timePeriods) {
        [self insertSegmentWithTitle:pair.string atIndex:i animated:NO];
        i += 1;
    }
}

- (NSInteger)selectedTimePeriod
{
    StringNumberPair *pair = self.timePeriods[self.selectedSegmentIndex];
    return [pair.number integerValue];
}

@end
