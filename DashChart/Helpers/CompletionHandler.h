//
//  CompletionHandler.h
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#ifndef CompletionHandler_h
#define CompletionHandler_h

typedef void (^CompletionHandler)(NSArray * _Nullable result, NSError * _Nullable error);

#endif /* CompletionHandler_h */
