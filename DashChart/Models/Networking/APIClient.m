//
//  APIClient.m
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import "APIClient.h"
#import "NSError+App.h"

@implementation APIClient

- (void)loadChartDataSince:(NSInteger)startTimestamp to:(NSInteger)endTimestamp completionHandler:(_Nonnull CompletionHandler)completionHandler;
{
    void (^completionHandlerOnMainThread)(NSArray *result, NSError * error) = ^(NSArray *result, NSError *error) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            completionHandler(result, error);
        });
    };

    NSString *baseURLString = @"https://dashpay.info/api/v0/chart_data";
    NSString *urlFormat = @"%@?start=%d&end=%d&exchange=poloniex&market=DASH_USDT";
    NSString *urlString = [NSString stringWithFormat:
                           urlFormat,
                           baseURLString,
                           startTimestamp,
                           endTimestamp];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSession *session = [NSURLSession sharedSession];

    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"%@", error);
            completionHandlerOnMainThread(nil, [NSError networkingErrorWhileLoadingChartData]);
            return;
        }
        NSError *jsonError;
        id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (jsonError) {
            NSLog(@"%@", jsonError);
            completionHandlerOnMainThread(nil, [NSError dataDecodingErrorForChartDataResponse]);
            return;
        }
        if (![jsonObject isKindOfClass:[NSArray class]]) {
            completionHandlerOnMainThread(nil, [NSError chartDataResponseIsNotArray]);
            return;
        }
        completionHandlerOnMainThread(jsonObject, nil);
    }];
    [task resume];
}

@end


