//
//  APIClient.h
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompletionHandler.h"

@interface APIClient : NSObject

- (void)loadChartDataSince:(NSInteger)startTimestamp to:(NSInteger)endTimestamp completionHandler:(_Nonnull CompletionHandler)completionHandler;

@end
