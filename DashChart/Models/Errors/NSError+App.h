//
//  NSError+App.h
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (App)

+ (NSError *)networkingErrorWhileLoadingChartData;
+ (NSError *)dataDecodingErrorForChartDataResponse;
+ (NSError *)chartDataResponseIsNotArray;
+ (NSError *)coreDataError;

@end
