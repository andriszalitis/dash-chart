//
//  NSError+App.m
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import "NSError+App.h"

@implementation NSError (App)

+ (NSError *)networkingErrorWhileLoadingChartData
{
    return [self appErrorWithCode:1
             localizedDescription:NSLocalizedString(@"Networking error while loading chart data", @"")];
}

+ (NSError *)dataDecodingErrorForChartDataResponse
{
    return [self appErrorWithCode:2
             localizedDescription:NSLocalizedString(@"Data decoding error while decoding chart data response", @"")];
}

+ (NSError *)chartDataResponseIsNotArray
{
    return [self appErrorWithCode:3
             localizedDescription:NSLocalizedString(@"Chart data response is not an array", @"")];
}

+ (NSError *)coreDataError
{
    return [self appErrorWithCode:4
             localizedDescription:NSLocalizedString(@"Core data error", @"")];
}

+ (NSError *)appErrorWithCode:(NSInteger)code localizedDescription:(NSString *)description
{
    NSDictionary *details = @{NSLocalizedDescriptionKey: description};
    NSError *error = [NSError errorWithDomain:@"App" code:code userInfo:details];
    return error;
}

@end
