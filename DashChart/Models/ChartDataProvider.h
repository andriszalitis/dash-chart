//
//  ChartDataProvider.h
//  DashChart
//
//  Created by Andris Zalitis on 09/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#ifndef ChartDataProvider_h
#define ChartDataProvider_h

@protocol ChartDataProvider

@property (nonatomic, readonly) NSArray *items;
@property (nonatomic, readonly, getter=isLoading) BOOL loading;
@property (nonatomic, readonly) NSError *error;

- (void)startLoadingSince:(NSDate *)startDate to:(NSDate *)endDate;

@end

#endif /* ChartDataProvider_h */
