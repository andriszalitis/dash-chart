//
//  LightweightChartDataItem.h
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface LightweightChartDataItem : NSObject

@property (nonatomic, assign) NSInteger timestamp;
@property (nonatomic, assign) double length;
@property (nonatomic, assign) double open;
@property (nonatomic, assign) double high;
@property (nonatomic, assign) double low;
@property (nonatomic, assign) double close;
@property (nonatomic, assign) double volume;
@property (nonatomic, assign) NSInteger lastAccessed;

+ (LightweightChartDataItem *)makeLightweightChartDataItemWithTimestamp:(NSInteger)timestamp
                                     lastAccessed:(NSInteger)lastAccessed;

+ (LightweightChartDataItem *)makeLightweightChartDataItemWithJSON:(NSDictionary *)json
                                lastAccessed:(NSInteger)lastAccessed;

@end
