//
//  ChartData.m
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import "ChartData.h"
#import "CompletionHandler.h"
#import "LightweightChartDataItem.h"
#import "APIClient.h"
#import "NSError+App.h"
#import "ChartDataItemStore.h"
#import "ChartDataItemNormalizer.h"

@interface NSDate (ConvenienceMethods)

- (NSInteger)secondsSince1970RoundedTo:(NSInteger)interval;

@end

@implementation NSDate (ConvenienceMethods)

- (NSInteger)secondsSince1970RoundedTo:(NSInteger)interval
{
    NSInteger secondsSince1970 = (NSInteger)[self timeIntervalSince1970];
    NSInteger numberOfIntervals = secondsSince1970 / interval;
    return numberOfIntervals * interval;
}

@end

@interface ChartData ()

@property (nonatomic, copy) NSArray *items;
@property (nonatomic, assign, getter=isLoading) BOOL loading;
@property (nonatomic, copy) NSError *error;
@property (nonatomic, strong) APIClient *apiClient;
@property (nonatomic, strong) ChartDataItemStore *store;

@end

@implementation ChartData

- (instancetype)init
{
    self = [super init];
    if (self) {
        _apiClient = [APIClient new];
        _store = [ChartDataItemStore new];
    }
    return self;
}

- (void)startLoadingSince:(NSDate *)startDate to:(NSDate *)endDate
{
    self.loading = YES;

    // API provides data for five minute ranges
    NSInteger secondsInFiveMinutes = 5 * 60;
    NSInteger startTimestamp = [startDate secondsSince1970RoundedTo:secondsInFiveMinutes];
    NSInteger endTimestamp = [endDate secondsSince1970RoundedTo:secondsInFiveMinutes];

    NSInteger secondsInRequestedRange = endTimestamp - startTimestamp;
    NSInteger numberOfItemsInRequestedRange = secondsInRequestedRange / secondsInFiveMinutes + 1;

    __weak __typeof(self)weakSelf = self;
    CompletionHandler networkLoadCompletion = ^(NSArray *jsonItems, NSError *error) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.loading = NO;
        if (error) {
            [strongSelf.class logError:error];
            strongSelf.error = error;
            return;
        }
        NSArray *items = [strongSelf createItemsFromJSONItems:jsonItems];
        double previousClosePrice = [strongSelf.store closePriceForTimestamp:startTimestamp - secondsInFiveMinutes];
        NSArray *normalizedItems = [ChartDataItemNormalizer normalizedItemsFrom:items
                                                                 startTimestamp:startTimestamp
                                                                   endTimestamp:endTimestamp
                                                                   itemDuration:secondsInFiveMinutes
                                                             previousClosePrice:previousClosePrice];
        strongSelf.items = [strongSelf.store storeItems:normalizedItems];
    };

    [self.store loadFromStoreSince:startTimestamp to:endTimestamp completionHandler:^(NSArray *items, NSError *error) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (error) {
            [strongSelf.apiClient loadChartDataSince:startTimestamp to:endTimestamp completionHandler:networkLoadCompletion];
            return;
        }
        if (items.count < numberOfItemsInRequestedRange) {
            [strongSelf.store deleteItems:items];
            [strongSelf.apiClient loadChartDataSince:startTimestamp to:endTimestamp completionHandler:networkLoadCompletion];
            return;
        }
        strongSelf.items = items;
        strongSelf.loading = NO;
    }];
}

#pragma mark -

- (NSArray *)createItemsFromJSONItems:(NSArray *)jsonItems
{
    NSInteger currentTimestamp = (NSInteger)[[NSDate new] timeIntervalSince1970];
    NSMutableArray *items = [NSMutableArray new];
    for (NSDictionary *jsonItem in jsonItems) {
        LightweightChartDataItem *item = [LightweightChartDataItem makeLightweightChartDataItemWithJSON:jsonItem lastAccessed:currentTimestamp];
        [items addObject:item];
    }
    return [items copy];
}

+ (void)logError:(NSError *)error
{
    if (error) {
        NSLog(@"%@", error);
    }
}

@end
