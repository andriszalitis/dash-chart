//
//  ChartData.h
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ChartDataProvider.h"

@interface ChartData : NSObject<ChartDataProvider>

@property (nonatomic, readonly) NSArray *items;
@property (nonatomic, readonly, getter=isLoading) BOOL loading;
@property (nonatomic, readonly) NSError *error;

- (void)startLoadingSince:(NSDate *)startDate to:(NSDate *)endDate;

@end
