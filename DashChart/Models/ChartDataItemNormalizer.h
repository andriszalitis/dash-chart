//
//  ChartDataItemNormalizer.h
//  DashChart
//
//  Created by Andris Zalitis on 08/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChartDataItemNormalizer : NSObject

+ (NSArray *)normalizedItemsFrom:(NSArray *)items
                  startTimestamp:(NSInteger)startTimestamp
                    endTimestamp:(NSInteger)endTimestamp
                    itemDuration:(NSInteger)itemDuration
              previousClosePrice:(double)previousClosePrice;

@end
