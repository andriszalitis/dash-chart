//
//  ChartDataItemNormalizer.m
//  DashChart
//
//  Created by Andris Zalitis on 08/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import "ChartDataItemNormalizer.h"
#import "LightweightChartDataItem.h"

@interface NSArray (ConvenienceMethods)

+ (NSArray *)strideFrom:(NSNumber *)startValue to:(NSNumber *)endValue generateNext:(NSNumber *(^)(NSNumber *previousNumber))generateNext;

@end

@implementation NSArray (ConvenienceMethods)

+ (NSArray *)strideFrom:(NSNumber *)startValue to:(NSNumber *)endValue generateNext:(NSNumber *(^)(NSNumber *previousNumber))generateNext
{
    NSMutableArray *values = [NSMutableArray new];
    NSNumber *value = startValue;
    while ([value compare:endValue] == kCFCompareLessThan) {
        [values addObject:value];
        value = generateNext(value);
    }
    return [values copy];
}

@end

@implementation ChartDataItemNormalizer

+ (NSArray *)normalizedItemsFrom:(NSArray *)items
                  startTimestamp:(NSInteger)startTimestamp
                    endTimestamp:(NSInteger)endTimestamp
                    itemDuration:(NSInteger)itemDuration
              previousClosePrice:(double)previousClosePrice
{
    NSArray *missingItems = [self generateMissingItemsForStartTimestamp:startTimestamp endTimestamp:endTimestamp itemInterval:itemDuration existingItems:items];

    NSArray *sortedItems = [self mergeExistingItems:items withMissingItems:missingItems];
    [self normalizePriceForItemsWithMissingPrice:sortedItems previousPrice:previousClosePrice];
    return sortedItems;
}

+ (NSArray *)generateMissingItemsForStartTimestamp:(NSInteger)startTimestamp
                                      endTimestamp:(NSInteger)endTimestamp
                                      itemInterval:(NSInteger)itemInterval
                                     existingItems:(NSArray *)existingItems
{
    NSArray *existingTimestamps = [existingItems valueForKey:@"timestamp"];
    NSArray *expectedTimestamps = [NSArray strideFrom:[NSNumber numberWithInteger:startTimestamp]
                                                   to:[NSNumber numberWithInteger:endTimestamp + 1]
                                         generateNext:^NSNumber *(NSNumber *previousNumber) {
                                             return [NSNumber numberWithInteger:[previousNumber integerValue] + itemInterval];
                                         }];
    NSMutableSet *missingTimestamps = [NSMutableSet setWithArray:expectedTimestamps];
    [missingTimestamps minusSet:[NSSet setWithArray:existingTimestamps]];

    NSMutableArray *items = [NSMutableArray new];
    NSInteger currentTimestamp = (NSInteger)[[NSDate new] timeIntervalSince1970];
    for (NSNumber *timestamp in missingTimestamps) {
        LightweightChartDataItem *chartDataItem = [LightweightChartDataItem makeLightweightChartDataItemWithTimestamp:[timestamp integerValue]
                                                                        lastAccessed:currentTimestamp];
        [items addObject:chartDataItem];
    }
    return items;
}

+ (NSArray *)mergeExistingItems:(NSArray *)existingItems withMissingItems:(NSArray *)missingItems
{
    NSMutableArray *allItems = [NSMutableArray arrayWithArray:existingItems];
    [allItems addObjectsFromArray:missingItems];
    NSSortDescriptor *timestampSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
    [allItems sortUsingDescriptors:@[timestampSortDescriptor]];
    return [allItems copy];
}

+ (void)normalizePriceForItemsWithMissingPrice:(NSArray *)items previousPrice:(double)previousPrice
{
    double lastPrice = 0;
    double firstPrice = previousPrice;
    for (LightweightChartDataItem *item in items) {
        if (item.open > 0 && firstPrice == 0) {
            firstPrice = item.open;
        }
        if (item.open == 0 && lastPrice > 0) {
            item.open = lastPrice;
            item.high = lastPrice;
            item.low = lastPrice;
            item.close = lastPrice;
        }
        lastPrice = item.close;
    }
    for (LightweightChartDataItem *item in items) {
        if (item.open != 0) {
            break;
        }
        item.open = firstPrice;
        item.high = firstPrice;
        item.low = firstPrice;
        item.close = firstPrice;
    }
}

@end
