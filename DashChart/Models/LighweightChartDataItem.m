//
//  LightweightChartDataItem.m
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import "LightweightChartDataItem.h"

@implementation LightweightChartDataItem

+ (LightweightChartDataItem *)makeLightweightChartDataItemWithTimestamp:(NSInteger)timestamp
                                     lastAccessed:(NSInteger)lastAccessed
{
    LightweightChartDataItem *item = [LightweightChartDataItem new];
    item.timestamp = timestamp;
    item.lastAccessed = lastAccessed;
    return item;
}

+ (LightweightChartDataItem *)makeLightweightChartDataItemWithJSON:(NSDictionary *)jsonItem
                                lastAccessed:(NSInteger)lastAccessed
{
    static dispatch_once_t onceToken;
    static NSISO8601DateFormatter *dateFormatter;
    dispatch_once(&onceToken, ^{
        dateFormatter = [NSISO8601DateFormatter new];
        dateFormatter.formatOptions = NSISO8601DateFormatWithFullDate | NSISO8601DateFormatWithDashSeparatorInDate | NSISO8601DateFormatWithTime | NSISO8601DateFormatWithColonSeparatorInTime | NSISO8601DateFormatWithFractionalSeconds | NSISO8601DateFormatWithTimeZone;
    });

    LightweightChartDataItem *item = [LightweightChartDataItem new];
    NSDate *time = [dateFormatter dateFromString:jsonItem[@"time"]];
    item.timestamp = (NSInteger)[time timeIntervalSince1970];
    item.open = [jsonItem[@"open"] doubleValue];
    item.high = [jsonItem[@"high"] doubleValue];
    item.low = [jsonItem[@"low"] doubleValue];
    item.close = [jsonItem[@"close"] doubleValue];
    item.volume = [jsonItem[@"pairVolume"] doubleValue];
    item.lastAccessed = lastAccessed;
    return item;
}

@end
