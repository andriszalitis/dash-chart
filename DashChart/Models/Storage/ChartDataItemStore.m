//
//  ChartDataItemStore.m
//  DashChart
//
//  Created by Andris Zalitis on 08/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import "ChartDataItemStore.h"
#import "AppDelegate.h"
#import "NSError+App.h"
#import "LightweightChartDataItem.h"
#import "StoredChartDataItem+CoreDataClass.h"

@interface NSManagedObjectContext (ConvenienceMethods)

- (NSError *)saveIfNeeded;

@end

@implementation NSManagedObjectContext (ConvenienceMethods)

- (NSError *)saveIfNeeded
{
    if (self.hasChanges) {
        NSError *error = nil;
        [self save:&error];
        return error;
    }
    return nil;
}

@end

@interface ChartDataItemStore ()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end

@implementation ChartDataItemStore

- (instancetype)init
{
    self = [super init];
    if (self) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        _managedObjectContext = [appDelegate.persistentContainer newBackgroundContext];
    }
    return self;
}

- (void)loadFromStoreSince:(NSInteger)startTimestamp to:(NSInteger)endTimestamp completionHandler:(_Nullable CompletionHandler)completionHandler
{
    if (!self.managedObjectContext) {
        completionHandler(nil, [NSError coreDataError]);
        return;
    }

    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"StoredChartDataItem"];
    NSSortDescriptor *timestampSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
    NSPredicate *timestampPredicate = [NSPredicate
                                       predicateWithFormat:@"(timestamp >= %@) && (timestamp <= %@)",
                                       [NSNumber numberWithInteger:startTimestamp],
                                       [NSNumber numberWithInteger:endTimestamp]
                                       ];
    fetchRequest.sortDescriptors = @[timestampSortDescriptor];
    fetchRequest.predicate = timestampPredicate;

    __weak __typeof(self)weakSelf = self;
    [self.managedObjectContext performBlock:^{
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        NSError *error = nil;
        NSArray *items = [strongSelf.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        dispatch_sync(dispatch_get_main_queue(), ^{
            completionHandler(items, error);
        });
    }];
}

- (void)deleteItems:(NSArray *)items
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    Class selfClass = self.class;
    __weak __typeof(self)weakSelf = self;
    [managedObjectContext performBlock:^{
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        for (StoredChartDataItem *item in items) {
            [managedObjectContext deleteObject:item];
        }
        NSError *error = [strongSelf.managedObjectContext saveIfNeeded];
        dispatch_sync(dispatch_get_main_queue(), ^{
            [selfClass logError:error];
        });
    }];
}

- (NSArray * _Nonnull)storeItems:(NSArray *)items
{
    NSMutableArray *storedItems = [NSMutableArray new];
    __weak __typeof(self)weakSelf = self;
    [self.managedObjectContext performBlockAndWait:^{
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        for (LightweightChartDataItem *item in items) {
            StoredChartDataItem *storedItem = [NSEntityDescription insertNewObjectForEntityForName:@"StoredChartDataItem" inManagedObjectContext:strongSelf.managedObjectContext];
            storedItem.timestamp = item.timestamp;
            storedItem.open = item.open;
            storedItem.high = item.high;
            storedItem.low = item.low;
            storedItem.close = item.close;
            storedItem.volume = item.volume;
            storedItem.lastAccessed = item.lastAccessed;
            [storedItems addObject:storedItem];
        }
    }];
    [self.managedObjectContext performBlock:^{
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        NSError *error = [strongSelf.managedObjectContext saveIfNeeded];
        [strongSelf.class logError:error];
    }];
    return storedItems;
}

- (void)saveChanges
{
    __weak __typeof(self)weakSelf = self;
    [self.managedObjectContext performBlock:^{
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        NSError *error = [strongSelf.managedObjectContext saveIfNeeded];
        [strongSelf.class logError:error];
    }];
}

- (double)closePriceForTimestamp:(NSTimeInterval)timestamp
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"StoredChartDataItem"];
    NSSortDescriptor *timestampSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
    NSPredicate *timestampPredicate = [NSPredicate
                                       predicateWithFormat:@"timestamp = %@",
                                       [NSNumber numberWithInteger:timestamp]
                                       ];
    fetchRequest.sortDescriptors = @[timestampSortDescriptor];
    fetchRequest.predicate = timestampPredicate;

    __block StoredChartDataItem *item = nil;
    __weak __typeof(self)weakSelf = self;
    [self.managedObjectContext performBlockAndWait:^{
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        NSError *error = nil;
        NSArray *items = [strongSelf.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        item = items.firstObject;
    }];
    if (item) {
        return item.close;
    } else {
        return 0;
    }
}

+ (void)logError:(NSError *)error
{
    if (error) {
        NSLog(@"%@", error);
    }
}

@end
