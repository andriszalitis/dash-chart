//
//  ChartDataItemStore.h
//  DashChart
//
//  Created by Andris Zalitis on 08/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "CompletionHandler.h"

@interface ChartDataItemStore : NSObject

- (void)loadFromStoreSince:(NSInteger)startTimestamp to:(NSInteger)endTimestamp completionHandler:(_Nullable CompletionHandler)completionHandler;
- (void)deleteItems:(NSArray * _Nonnull)items;
- (NSArray * _Nonnull)storeItems:(NSArray * _Nonnull)items;
- (double)closePriceForTimestamp:(NSTimeInterval)timestamp;

@end
