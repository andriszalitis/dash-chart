//
//  ChartPresenter.m
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import "ChartPresenter.h"
#import "ChartData.h"
#import "StoredChartDataItem+CoreDataClass.h"
#import "ChartYValue.h"

@interface ChartPresenter ()

@property (nonatomic, assign) NSInteger selectedTimeInterval;
@property (nonatomic, assign) NSInteger selectedBarWidth;

@end

@implementation ChartPresenter

static void *ChartDataItemsContext = &ChartDataItemsContext;
static void *ChartDataErrorContext = &ChartDataErrorContext;
static void *ChartDataLoadingContext = &ChartDataLoadingContext;

- (instancetype)initWithSelectedTimeInterval:(NSInteger)selectedTimeInterval
                            selectedBarWidth:(NSInteger)selectedBarWidth
{
    self = [super init];
    if (self) {
        _chartData = [ChartData new];
        _selectedTimeInterval = selectedTimeInterval;
        _selectedBarWidth = selectedBarWidth;
    }
    return self;
}

- (void)start
{
    [self.chartData addObserver:self
                forKeyPath:@"items"
                   options:NSKeyValueObservingOptionNew
                   context:ChartDataItemsContext];
    [self.chartData addObserver:self
                forKeyPath:@"error"
                   options:NSKeyValueObservingOptionNew
                   context:ChartDataErrorContext];
    [self.chartData addObserver:self
                forKeyPath:@"loading"
                   options:NSKeyValueObservingOptionNew
                   context:ChartDataLoadingContext];

    [self updateData];
}

- (void)updateData
{
    NSDate *now = [NSDate new];
    NSDate *startDate = [now dateByAddingTimeInterval:-self.selectedTimeInterval];
    [self.chartData startLoadingSince:startDate to:now];
}

- (void)didChangeValueOfTimeIntervalTo:(NSInteger)timeInterval
{
    self.selectedTimeInterval = timeInterval;
    [self updateData];
}

- (void)didChangeValueOfBarWidthTo:(NSInteger)barWidth
{
    self.selectedBarWidth = barWidth;
    [self updateData];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (context == ChartDataItemsContext) {
        NSArray *yValues = [self generateChartYValuesForDataItems:self.chartData.items];
        NSArray *xValues = [self generateChartXValuesForDataItems:self.chartData.items];
        [self.chartView presentChartWithYValues:yValues xValues:xValues];
    } else if (context == ChartDataErrorContext) {
        // do nothing for now
    } else if (context == ChartDataLoadingContext) {
        if (self.chartData.isLoading) {
            [self.chartView showLoadingIndicator];
        } else {
            [self.chartView hideLoadingIndicator];
        }
    } else {
        [super observeValueForKeyPath:keyPath
                             ofObject:object
                               change:change
                              context:context];
    }
}

- (NSArray *)generateChartYValuesForDataItems:(NSArray *)dataItems
{
    NSMutableArray *yValues = [NSMutableArray new];
    for (StoredChartDataItem *dataItem in dataItems) {
        ChartYValue *yValue = [ChartYValue new];
        yValue.open = dataItem.open;
        yValue.high = dataItem.high;
        yValue.low = dataItem.low;
        yValue.close = dataItem.close;
        yValue.volume = dataItem.volume;
        [yValues addObject:yValue];
    }
    return [yValues copy];
}

- (NSArray *)generateChartXValuesForDataItems:(NSArray *)dataItems
{
    NSMutableArray *xValues = [NSMutableArray new];
    for (StoredChartDataItem *dataItem in dataItems) {
        [xValues addObject:[NSNumber numberWithInteger:dataItem.timestamp]];
    }
    return [xValues copy];
}

- (void)stop
{
    [self.chartData removeObserver:self forKeyPath:@"items" context:ChartDataItemsContext];
    [self.chartData removeObserver:self forKeyPath:@"error" context:ChartDataErrorContext];
    [self.chartData removeObserver:self forKeyPath:@"loading" context:ChartDataLoadingContext];
}

@end
