//
//  ChartPresenter.h
//  DashChart
//
//  Created by Andris Zalitis on 07/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ChartView.h"
#import "ChartDataProvider.h"

@interface ChartPresenter : NSObject

@property (nonatomic, strong) NSObject<ChartDataProvider> *chartData;
@property (nonatomic, weak) id<ChartView> chartView;

- (instancetype)initWithSelectedTimeInterval:(NSInteger)selectedTimeInterval
                            selectedBarWidth:(NSInteger)selectedBarWidth;
- (void)start;
- (void)didChangeValueOfTimeIntervalTo:(NSInteger)timeInterval;
- (void)didChangeValueOfBarWidthTo:(NSInteger)barWidth;
- (void)stop;

@end
