//
//  ChartPresenterTests.m
//  ChartPresenterTests
//
//  Created by Andris Zalitis on 06/01/2018.
//  Copyright © 2018 Andris Zalitis. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ChartPresenter.h"
#import "ChartDataProvider.h"

@interface ChartDataSpy : NSObject<ChartDataProvider>

@property (nonatomic, readonly) NSArray *items;
@property (nonatomic, readonly, getter=isLoading) BOOL loading;
@property (nonatomic, readonly) NSError *error;

@property (nonatomic, copy) NSMutableArray *capturedArgumentsOfStartLoading;

- (void)startLoadingSince:(NSDate *)startDate to:(NSDate *)endDate;

@end

@implementation ChartDataSpy

- (instancetype)init
{
    self = [super init];
    if (self) {
        _capturedArgumentsOfStartLoading = [NSMutableArray new];
    }
    return self;
}

- (void)startLoadingSince:(NSDate *)startDate to:(NSDate *)endDate
{
    [self.capturedArgumentsOfStartLoading addObject:@[startDate, endDate]];
}

@end

@interface NSDate (EqualWithAccuracy)

- (BOOL)isEqualToDate:(NSDate *)otherDate withAccuracy:(NSTimeInterval)accuracy;

@end

@implementation NSDate (EqualWithAccuracy)

- (BOOL)isEqualToDate:(NSDate *)otherDate withAccuracy:(NSTimeInterval)accuracy
{
    NSDate *date1 = [otherDate dateByAddingTimeInterval:-accuracy];
    NSDate *date2 = [otherDate dateByAddingTimeInterval:accuracy];
    return [self compare:date1] != kCFCompareLessThan && [self compare:date2] != kCFCompareGreaterThan;
}

@end

@interface ChartPresenterTests : XCTestCase

@end

@implementation ChartPresenterTests

- (void)testLoadsChartDataOnStart1 {
    ChartPresenter *chartPresenter = [[ChartPresenter alloc] initWithSelectedTimeInterval:10 selectedBarWidth:1];
    ChartDataSpy *chartDataSpy = [ChartDataSpy new];
    chartPresenter.chartData = chartDataSpy;

    [chartPresenter start];

    NSDate *now = [NSDate new];
    NSArray *expectedStartLoadingArguments = @[[now dateByAddingTimeInterval:-10], now];
    XCTAssertEqual(chartDataSpy.capturedArgumentsOfStartLoading.count, 1);
    XCTAssertTrue([chartDataSpy.capturedArgumentsOfStartLoading.firstObject[0] isEqualToDate:expectedStartLoadingArguments[0] withAccuracy:0.1]);
    XCTAssertTrue([chartDataSpy.capturedArgumentsOfStartLoading.firstObject[1] isEqualToDate:expectedStartLoadingArguments[1] withAccuracy:0.1]);
}

- (void)testLoadsChartDataOnStart2 {
    ChartPresenter *chartPresenter = [[ChartPresenter alloc] initWithSelectedTimeInterval:100 selectedBarWidth:2];
    ChartDataSpy *chartDataSpy = [ChartDataSpy new];
    chartPresenter.chartData = chartDataSpy;

    [chartPresenter start];

    NSDate *now = [NSDate new];
    NSArray *expectedStartLoadingArguments = @[[now dateByAddingTimeInterval:-100], now];
    XCTAssertEqual(chartDataSpy.capturedArgumentsOfStartLoading.count, 1);
    XCTAssertTrue([chartDataSpy.capturedArgumentsOfStartLoading.firstObject[0] isEqualToDate:expectedStartLoadingArguments[0] withAccuracy:0.1]);
    XCTAssertTrue([chartDataSpy.capturedArgumentsOfStartLoading.firstObject[1] isEqualToDate:expectedStartLoadingArguments[1] withAccuracy:0.1]);
}

@end

